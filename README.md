Nisum Test

Gradle: 8.1

Java: 8

Spring Boot: 2.5.5

1- Clonar el repo e importar el proyecto en nuestro IDE

2- Instalar las dependencias con gradle

3- Ejecutar y probar

Endpoint para registrar usuarios: http://localhost:8080/users/register

Request ejemplo:

```
{
    "name": "Enzo Geron",
    "email": "enzogeron@gmail.com",
    "password": "Prueba9821!",
    "phones": [
        {
            "number": "47219901",
            "citycode": "22",
            "countrycode": "55"
        },
        {
            "number": "54768470",
            "citycode": "11",
            "countrycode": "54"
        }
    ]
}
```

Importante: la password debe ser minimo de 8 caracteres, tambien debe contener 1 letra mayuscula, 1 minuscula, 1 numero y 1 caracter especial.

Swagger: http://localhost:8080/swagger-ui/index.html

URL para probar H2: http://localhost:8080/h2-console/

```
Driver Class: org.h2.Driver
JDBC URL: jdbc:h2:mem:nisum
Username: nisum
```

