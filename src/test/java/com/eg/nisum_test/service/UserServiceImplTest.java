package com.eg.nisum_test.service;

import com.eg.nisum_test.dto.PhoneDTO;
import com.eg.nisum_test.dto.UserDTO;
import com.eg.nisum_test.dto.UserResponseDTO;
import com.eg.nisum_test.entity.User;
import com.eg.nisum_test.exception.BadRequestException;
import com.eg.nisum_test.repository.UserRepository;
import com.eg.nisum_test.util.JwtUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private JwtUtil jwtUtil;

    @BeforeEach
    public void setUp() {
        String validationPasswordRegex = "validationPasswordRegex";
        String regex = "^(?=.*[!@#$%^&*()_+\\-=\\[\\]{};':\"\\\\|,.<>\\/?])(?=.*[A-Z])(?=.*[a-z])(?=.*\\d).{8,}$";
        ReflectionTestUtils.setField(userService, validationPasswordRegex, regex);
    }

    @Test
    public void testRegisterUser() {
        UserDTO userDTO = new UserDTO();
        userDTO.setName("Enzo Geron");
        userDTO.setEmail("enzogeron@gmail.com");
        userDTO.setPassword("Prueba1234-");

        List<PhoneDTO> phonesDto = new ArrayList<>();
        PhoneDTO phone01 = new PhoneDTO();
        phone01.setNumber("54768470");
        phone01.setCitycode("11");
        phone01.setCountrycode("AR");

        phonesDto.add(phone01);
        userDTO.setPhones(phonesDto);

        when(userRepository.existsByEmail(any(String.class))).thenReturn(false);

        String token = "xxxxxxxxxxxx";
        when(jwtUtil.generateToken(any(UUID.class))).thenReturn(token);

        User user = new User();
        user.setId(UUID.fromString("0c5c9a4a-6df8-4d5b-ad57-a18dbc1098f2"));
        user.setName("Enzo Geron");
        user.setEmail("enzogeron@gmail.com");
        user.setPassword("$2a$10$QoQX8QPSzCRzwGgHegr2SOQVfNKV7l4h3kWtyN.4Um0s5Zn5JsAj2");
        user.setCreatedAt(new Date());
        user.setUpdatedAt(new Date());
        user.setLastLogin(new Date());
        user.setActive(true);

        when(userRepository.save(any(User.class))).thenReturn(user);

        UserResponseDTO userResponseDTO = userService.register(userDTO);

        assertNotNull(userResponseDTO.getId());
        assertEquals(token, userResponseDTO.getToken());
        assertEquals(userDTO.getEmail(), userResponseDTO.getEmail());

    }

    @Test
    public void testCreateUserWithExistingEmail() {
        UserDTO userDTO = new UserDTO();
        userDTO.setName("Enzo Geron");
        userDTO.setEmail("enzogeron@gmail.com");

        when(userRepository.existsByEmail(any(String.class))).thenReturn(true);

        assertThrows(BadRequestException.class, () -> {
            userService.register(userDTO);
        });
    }

    @Test
    public void testCreateUserWithPasswordNotValid() {
        UserDTO userDTO = new UserDTO();
        userDTO.setName("Enzo Geron");
        userDTO.setEmail("enzogeron@gmail.com");
        userDTO.setPassword("1234");

        when(userRepository.existsByEmail(any(String.class))).thenReturn(true);

        assertThrows(BadRequestException.class, () -> {
            userService.register(userDTO);
        });
    }

}


