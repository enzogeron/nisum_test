package com.eg.nisum_test.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidator {

    public static boolean isValid(String password) {
        String regex = "^(?=.*[!@#$%^&*()_+\\-=\\[\\]{};':\"\\\\|,.<>\\/?])(?=.*[A-Z])(?=.*[a-z])(?=.*\\d).{8,}$";
        return PasswordValidator.check(password, regex);
    }

    public static boolean isValid(String password, String regex) {
        return PasswordValidator.check(password, regex);
    }

    private static boolean check(String password, String regex) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }
}
