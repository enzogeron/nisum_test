package com.eg.nisum_test.dto;

import com.eg.nisum_test.entity.Phone;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class UserResponseDTOBuilder {

    private UUID id;
    private String name;
    private String email;

    private Date created;

    private Date modified;

    private Date lastLogin;

    private String token;

    private Boolean isActive;

    public UserResponseDTOBuilder withId(UUID id) {
        this.id = id;
        return this;
    }

    public UserResponseDTOBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public UserResponseDTOBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    public UserResponseDTOBuilder withCreated(Date created) {
        this.created = created;
        return this;
    }

    public UserResponseDTOBuilder withModified(Date modified) {
        this.modified = modified;
        return this;
    }

    public UserResponseDTOBuilder withLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
        return this;
    }

    public UserResponseDTOBuilder withToken(String token) {
        this.token = token;
        return this;
    }

    public UserResponseDTOBuilder withIsActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public UserResponseDTO build() {
        return new UserResponseDTO(id, name, email, created, modified, lastLogin, token, isActive);
    }
}
