package com.eg.nisum_test.dto;

import com.eg.nisum_test.entity.Phone;

import java.util.Date;
import java.util.List;
import java.util.UUID;

public class UserResponseDTO {
    private UUID id;
    private String name;
    private String email;

    private Date created;

    private Date modified;

    private Date lastLogin;

    private String token;

    private Boolean isActive;

    public UserResponseDTO(UUID id, String name, String email, Date created, Date modified, Date lastLogin, String token, Boolean isActive) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.created = created;
        this.modified = modified;
        this.lastLogin = lastLogin;
        this.token = token;
        this.isActive = isActive;
    }

    public UserResponseDTO() {}

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public String getToken() {
        return token;
    }

    public Boolean getActive() {
        return isActive;
    }

    public static UserResponseDTOBuilder builder() {
        return new UserResponseDTOBuilder();
    }
}
