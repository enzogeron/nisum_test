package com.eg.nisum_test.service;

import com.eg.nisum_test.dto.UserDTO;
import com.eg.nisum_test.dto.UserResponseDTO;
import com.eg.nisum_test.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;

public interface UserService {

    UserResponseDTO register(UserDTO user);
    List<User> getUsers();

}
