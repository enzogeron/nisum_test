package com.eg.nisum_test.service;

import com.eg.nisum_test.common.EmailValidator;
import com.eg.nisum_test.common.PasswordValidator;
import com.eg.nisum_test.dto.PhoneDTO;
import com.eg.nisum_test.dto.UserDTO;
import com.eg.nisum_test.dto.UserResponseDTO;
import com.eg.nisum_test.entity.Phone;
import com.eg.nisum_test.entity.User;
import com.eg.nisum_test.exception.EmailAlreadyExistsException;
import com.eg.nisum_test.exception.InvalidEmailException;
import com.eg.nisum_test.exception.InvalidPasswordException;
import com.eg.nisum_test.repository.UserRepository;
import com.eg.nisum_test.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtUtil jwtUtil;

    @Value("${validation.password.regex}")
    private String validationPasswordRegex;

    @Override
    public UserResponseDTO register(UserDTO userDTO) {
        validateUserDTO(userDTO);

        User user = createUserFromDTO(userDTO);
        User userSaved = userRepository.save(user);
        String jwtToken = jwtUtil.generateToken(userSaved.getId());

        UserResponseDTO userResponseDTO = UserResponseDTO.builder()
                .withId(userSaved.getId())
                .withName(userSaved.getName())
                .withEmail(userDTO.getEmail())
                .withCreated(userSaved.getCreatedAt())
                .withModified(userSaved.getUpdatedAt())
                .withLastLogin(userSaved.getLastLogin())
                .withToken(jwtToken)
                .withIsActive(userSaved.getActive()).build();

        return userResponseDTO;
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    private void validateUserDTO(UserDTO userDTO) {
        String email = userDTO.getEmail();

        if(userRepository.existsByEmail(email)) {
            throw new EmailAlreadyExistsException("El email ya esta registrado.");
        }

        if(!EmailValidator.isValid(email)) {
            throw new InvalidEmailException("El email no es valido.");
        }

        String password = userDTO.getPassword();

        if(!PasswordValidator.isValid(password, this.validationPasswordRegex)) {
            throw new InvalidPasswordException("La password no es valida.");
        }
    }

    private User createUserFromDTO(UserDTO userDTO) {
        User user = new User();
        user.setName(userDTO.getName());
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        user.setActive(true);

        List<Phone> phones = userDTO.getPhones().stream().map(this::createPhoneFromDTO).collect(Collectors.toList());
        phones.forEach(phone -> phone.setUser(user));
        user.setPhones(phones);

        return user;
    }

    private Phone createPhoneFromDTO(PhoneDTO phoneDTO) {
        Phone phone = new Phone();
        phone.setNumber(phoneDTO.getNumber());
        phone.setCityCode(phoneDTO.getCitycode());
        phone.setCountryCode(phoneDTO.getCountrycode());
        return phone;
    }
}
